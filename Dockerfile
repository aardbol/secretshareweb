FROM caddy:2.4.6-alpine

#ARG UNAME=secretshareweb
#ARG UID=10006
#ARG GID=10006
ARG WORKDIR=/srv

#RUN addgroup --gid $GID $UNAME \
#    && adduser -D --uid $UID --ingroup $UNAME --shell /bin/bash $UNAME \
#    && chown -R $UID:$GID $WORKDIR
#USER $UNAME

COPY Caddyfile /etc/caddy/Caddyfile

WORKDIR $WORKDIR
COPY static/ static/
COPY *.html favicon.ico ./

RUN find $WORKDIR -type d -exec chmod 755 {} \; && find $WORKDIR -type f -exec chmod 644 {} \;

EXPOSE 443